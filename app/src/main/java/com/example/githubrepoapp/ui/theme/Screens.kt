package com.example.githubrepoapp.ui.theme

sealed class Screen(val route : String) {

    object GitRepoListScreen : Screen("git_repo_list_screen")
    object GitRepoDetailsScreen : Screen("git_repo_details_screen")
}