package com.example.githubrepoapp.presentation.module

import com.example.githubrepoapp.data.api.ApiConstants
import com.example.githubrepoapp.data.api.ApiService
import com.example.githubrepoapp.data.repository.RemoteRepositoryImpl
import com.example.githubrepoapp.data.repository.datasource.DataSource
import com.example.githubrepoapp.data.repository.datasourceimpl.DataSourceImpl
import com.example.githubrepoapp.domain.repository.RemoteRepository
import com.example.githubrepoapp.domain.usecase.UseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MyModule {

    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: ApiService): DataSource {
        return DataSourceImpl(apiService)
    }

    @Provides
    @Singleton
    fun provideApiRepository(dataSource: DataSource): RemoteRepository {
        return RemoteRepositoryImpl(dataSource)
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        val builder = OkHttpClient.Builder()

        builder.addInterceptor(Interceptor { chain ->
            val requestBuilder = chain.request().newBuilder()
            requestBuilder.header("Package", "com.example.githubrepoapp")
            chain.proceed(requestBuilder.build())
        })

        val client: OkHttpClient = builder.build()
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .baseUrl(ApiConstants.BASE_URL)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideUseCase(remoteRepository: RemoteRepository): UseCase {
        return UseCase(remoteRepository)
    }

}