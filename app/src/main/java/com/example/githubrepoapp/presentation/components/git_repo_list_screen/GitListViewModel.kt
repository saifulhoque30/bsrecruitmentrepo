package com.example.githubrepoapp.presentation.components.git_repo_list_screen

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubrepoapp.domain.usecase.UseCase
import com.example.githubrepoapp.presentation.components.git_repo_list_screen.states.GitListDataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GitListViewModel @Inject constructor(private val useCase: UseCase) : ViewModel() {
    private var getDataJob: Job? = null
    private val _state = MutableStateFlow(GitListDataState())
    val state = _state.asStateFlow()
    init {
        getData()
    }

    private fun getData() {
        getDataJob?.cancel()
        getDataJob = viewModelScope.launch(Dispatchers.IO) {
            val dataList = useCase.getData()
            _state.value = _state.value.copy(
                dataList = dataList.items
            )
        }
    }
}