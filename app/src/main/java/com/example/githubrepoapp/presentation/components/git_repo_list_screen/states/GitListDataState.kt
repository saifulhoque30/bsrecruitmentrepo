package com.example.githubrepoapp.presentation.components.git_repo_list_screen.states

import com.example.githubrepoapp.data.model.Item

data class GitListDataState(
    val dataList: List<Item> = emptyList()
)