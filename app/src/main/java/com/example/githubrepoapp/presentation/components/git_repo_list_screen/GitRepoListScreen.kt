package com.example.githubrepoapp.presentation.components.git_repo_list_screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.example.githubrepoapp.data.model.Item
import com.example.githubrepoapp.ui.theme.Screen
import com.google.gson.Gson

@Composable
fun GitRepoListScreen(
    navController: NavHostController,
    viewModel: GitListViewModel = hiltViewModel()
) {
    val context = LocalContext.current
    val state by viewModel.state.collectAsState()

    ReposList(state.dataList,navController)
}
@Composable
fun ReposListItem(navController: NavHostController,modifier: Modifier = Modifier, item: Item) {
    Card(
        shape = RoundedCornerShape(5.dp),
        modifier = modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(10.dp)
            .clickable {
             //todo navigate to Details
            }
    ) {
        Column(
            horizontalAlignment = Alignment.Start,
            modifier = Modifier
                .padding(10.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = rememberAsyncImagePainter(item.owner.avatar_url),
                    contentDescription = null,
                    modifier = Modifier.size(size = 20.dp)
                )
                Spacer(modifier = Modifier.size(height = 0.dp, width = 5.dp))
                Text(text = item.name?.substringBefore(delimiter = "/") ?: "N/A", fontSize = 16.sp)
            }
            Spacer(modifier = Modifier.height(5.dp))
            Text(text = "Name: ${item.full_name ?: "N/A"}", fontSize = 18.sp, fontWeight = FontWeight.Bold)
            Spacer(modifier = Modifier.height(5.dp))
            Text(text = "Description: ${item.description?: "N/A"}", fontSize = 14.sp)
        }
    }
}

@Composable
fun ReposList(items: List<Item>, navController: NavHostController) {
    LazyColumn {
        if (items.isNotEmpty()) {
            itemsIndexed(items) { index, data ->
                ReposListItem(item = data, navController = navController)
            }
        }
    }
}