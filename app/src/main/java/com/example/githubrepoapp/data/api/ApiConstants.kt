package com.example.githubrepoapp.data.api

class ApiConstants {
    companion object {
        const val BASE_URL: String = "https://api.github.com/"
    }
}