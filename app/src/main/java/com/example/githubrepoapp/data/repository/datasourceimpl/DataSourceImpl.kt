package com.example.githubrepoapp.data.repository.datasourceimpl

import com.example.githubrepoapp.data.api.ApiService
import com.example.githubrepoapp.data.model.ResponseData
import com.example.githubrepoapp.data.repository.datasource.DataSource

class DataSourceImpl(private val apiService: ApiService) : DataSource {
    override suspend fun getDataList(): ResponseData {
        return apiService.searchRepos("android","asc")
    }
}