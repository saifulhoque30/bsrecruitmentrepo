package com.example.githubrepoapp.data.api

import com.example.githubrepoapp.data.model.ResponseData
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
     @GET("search/repositories")
     suspend fun searchRepos(
          @Query("q") query: String,
          @Query("sort") sort: String
     ): ResponseData
}