package com.example.githubrepoapp.data.repository.datasource

import com.example.githubrepoapp.data.model.ResponseData

interface DataSource {
    suspend fun getDataList(): ResponseData
}