package com.example.githubrepoapp.data.repository

import com.example.githubrepoapp.data.model.ResponseData
import com.example.githubrepoapp.data.repository.datasource.DataSource
import com.example.githubrepoapp.domain.repository.RemoteRepository


class RemoteRepositoryImpl(private val dataSource: DataSource) : RemoteRepository {
   override suspend fun getData(): ResponseData {
       return dataSource.getDataList()
    }
}