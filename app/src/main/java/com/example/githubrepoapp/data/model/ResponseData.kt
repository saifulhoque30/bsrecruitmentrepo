package com.example.githubrepoapp.data.model

data class ResponseData(
    val incomplete_results: Boolean,
    val items: List<Item>,
    val total_count: Int
)