package com.example.githubrepoapp

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.githubrepoapp.presentation.components.git_repo_list_screen.GitRepoListScreen
import com.example.githubrepoapp.ui.theme.Screen

@Composable

fun Navigation(navController: NavController){

    NavHost(navController = navController as NavHostController, startDestination = Screen.GitRepoListScreen.route){
      composable(route = Screen.GitRepoListScreen.route){
          GitRepoListScreen(navController = navController)
      }
    }
}