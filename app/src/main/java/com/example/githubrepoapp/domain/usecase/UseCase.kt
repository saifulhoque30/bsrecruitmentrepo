package com.example.githubrepoapp.domain.usecase

import com.example.githubrepoapp.data.model.ResponseData
import com.example.githubrepoapp.domain.repository.RemoteRepository

class UseCase(private val remoteRepository: RemoteRepository) {

    suspend fun getData() : ResponseData {
        return remoteRepository.getData()
    }
}