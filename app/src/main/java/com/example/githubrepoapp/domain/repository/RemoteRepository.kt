package com.example.githubrepoapp.domain.repository

import com.example.githubrepoapp.data.model.ResponseData

interface RemoteRepository {
    suspend fun getData() : ResponseData
}